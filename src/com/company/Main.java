package com.company;

public class Main {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        Square square1 = new Square();
        Square square2 = new Square();
        
        rectangle.setBreadth(5);
        rectangle.setLength(4);
        System.out.println("Area Rectangle: " + rectangle.getArea());

        square1.setLength(4);
        System.out.println("Area Square1: " + square1.getArea());

        square2.setBreadth(4);
        System.out.println("Area Square2: " + square2.getArea());

    }
}
